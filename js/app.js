/* declarar variables en JavaScript*/ 

let numero=30; /*no hay enteros flotantes, y let sirve para declarar una variable */
var nombre = "Jose lopez"
const PI= 3.1416;
function arribaMouse(){
let parrafo = document.getElementById('parrafo');
parrafo.style.color = 'blue';
parrafo.style.fontSize = '24px';
parrafo.style.textAlign = 'justify';
}

function salirMouse(){
let parrafo = document.getElementById('parrafo');
parrafo.style.color = "red";
parrafo.style.fontSize = "17px";
parrafo.style.textAlign = "left";
}

function arribaMouse2(){
    let parrafo = document.getElementById('parrafo2');
    parrafo.style.color = 'green';
    parrafo.style.fontSize = '30px';
    parrafo.style.textAlign = 'justify';
}

function salirMouse2(){
    let parrafo2 = document.getElementById('parrafo2');
    parrafo2.style.color = 'brown';
    parrafo2.style.fontSize = '25px';
    parrafo2.style.textAlign = 'left';
}
function arribaMouse3(){
    let parrafo = document.getElementById('titulo');
    parrafo.style.color = 'purple';
    parrafo.style.fontSize = '30px';
    parrafo.style.textAlign = 'center';
}

function salirMouse3(){
    let parrafo2 = document.getElementById('titulo');
    parrafo2.style.color = 'gray';
    parrafo2.style.fontSize = '25px';
    parrafo2.style.textAlign = 'center';
}
function limpiarParrafo(){
    let parrafo = document.getElementById('parrafo');
    parrafo.innerHTML = "Buenos dias, buenas tardes y buenas noches";
}