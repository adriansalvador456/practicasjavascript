function mostrarTabla() {
    var select = document.getElementById('cmbTablas');
    var tabla = document.getElementById('tablaMultiplicacion');
    tabla.innerHTML = '';
  
    var numeroTabla = parseInt(select.value);
    var arreglo = [];
  

    for (var i = 1; i <= 10; i++) {
      arreglo.push(numeroTabla * i);
    }
  
    var tablaHTML = '<table>';
    for (var j = 0; j < arreglo.length; j++) {
      tablaHTML += '<tr>'; 
      tablaHTML += '<td><img src="/img/' + select.value + '.png" alt="Imagen ' + select.value + '"></td>';
      tablaHTML += '<td><img src="/img/multi.png" alt="x"></td>';
      tablaHTML += '<td><img src="/img/' + (j + 1) + '.png" alt="Imagen ' + (j + 1) + '"></td>';
      tablaHTML += '<td><img src="/img/igual.png" alt="="></td>';
      var resultado = (select.value * (j + 1)).toString();
      for (var k = 0; k < resultado.length; k++) {
      tablaHTML += '<td><img src="/img/' + resultado[k] + '.png" alt="Imagen ' + resultado[k] + '"></td>';
    }
    tablaHTML += '</tr>';
}
tablaHTML += '</table>';
tabla.innerHTML = tablaHTML;
}