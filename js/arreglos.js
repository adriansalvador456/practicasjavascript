function generarNumeros(cantidad) {
    var numeros = [];
    for (var i = 0; i < cantidad; i++) {
      numeros.push(Math.floor(Math.random() * 100)); 
    }
    return numeros;
  }
  
  function mostrarElementos(array) {
    var lblElementos = document.getElementById("lblElementos");
    lblElementos.innerHTML = array.join(", ");
  }
  
  
  function mostrarNumerosPares(array) {
    var lblNumerosPares = document.getElementById("lblNumerosPares");
    var numerosPares = array.filter(function(num) {
      return num % 2 === 0;
    });
    lblNumerosPares.innerHTML = numerosPares.join(", ");
  }
  

  function mostrarMayor(array) {
    var mayor = array[0];
    var posicion = 1;
    for (var i = 1; i < array.length; i++) {
      if (array[i] > mayor) {
        mayor = array[i];
        posicion = i + 1;
      }
    }
    var lblMayor = document.getElementById("lblMayor");
    lblMayor.innerHTML = "Valor mayor: " + mayor + ", Posición: " + posicion;
  }
  

  function mostrarPromedio(array) {
    var suma = array.reduce(function(acc, num) {
      return acc + num;
    }, 0);
    var promedio = suma / array.length;
    var lblPromedio = document.getElementById("lblPromedio");
    lblPromedio.innerHTML = "Promedio: " + promedio;
  }
  function mostrarMenor(array) {
    var menor = array[0];
    var posicion = 1;
    for (var i = 1; i < array.length; i++) {
      if (array[i] < menor) {
        menor = array[i];
        posicion = i + 1;
      }
    }
    var lblMenor = document.getElementById("lblMenor");
    lblMenor.innerHTML = "Valor menor: " + menor + ", Posición: " + posicion;
  }
  
  function mostrarPorcentajeSimetria(array) {
    var simetricos = 0;
    for (var i = 0; i < array.length / 2; i++) {
      if (array[i] === array[array.length - 1 - i]) {
        simetricos++;
      }
    }
    var porcentaje = (simetricos / array.length) * 100;
    var lblSimetria = document.getElementById("lblSimetria");
    lblSimetria.innerHTML = "Porcentaje de simetría: " + porcentaje + "%";
  }
  

  function Generar() {
    var cantidadNumeros = parseInt(document.getElementById("txtNumeros").value);
    var numerosGenerados = generarNumeros(cantidadNumeros);
    mostrarElementos(numerosGenerados);
    mostrarNumerosPares(numerosGenerados);
    mostrarMayor(numerosGenerados);
    mostrarPromedio(numerosGenerados);
    mostrarMenor(numerosGenerados);
    mostrarPorcentajeSimetria(numerosGenerados);
  }