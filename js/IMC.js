const btnCalcularIMC = document.getElementById('btnCalcularIMC');
btnCalcularIMC.addEventListener('click', function () {
  let edad = parseInt(document.getElementById('txtEdad').value);
  let altura = parseFloat(document.getElementById('txtAltura').value);
  let peso = parseFloat(document.getElementById('txtPeso').value);
  const imc = peso / (altura * altura);
  const generoMasculino = document.getElementById('sexoMasculino').checked;
  const generoFemenino = document.getElementById('sexoFemenino').checked;
  const genero = generoMasculino ? 'sexoMasculino' : generoFemenino ? 'sexoFemenino' : '';
  const ventanaResultado = window.open("", "Resultado del IMC", `width=${screen.width},height=${screen.height},left=0,top=0`);  ventanaResultado.document.open();
  ventanaResultado.document.close();
  ventanaResultado.document.write(`<style>body { background-color: white; text-align: center; }</style>`);
  ventanaResultado.document.write(`<div style="background-color: aquamarine; padding: 3px;">
  <h1 style="font-family: 'Roboto';">Bienvenido, Sus resultados fueron:</h1>
  </div>`);
  ventanaResultado.document.write(`<h1 style="font-family: 'Roboto';">Edad: ${edad}<h1>`);
  ventanaResultado.document.write(`<h1 style="font-family: 'Roboto';">Altura: ${altura}</h1>`);
  ventanaResultado.document.write(`<h1 style="font-family: 'Roboto';">Peso: ${peso}</h1>`);
  ventanaResultado.document.write(`<h1 style="font-family: 'Roboto';">Tu IMC es: ${imc.toFixed(2)}</h1>`);
  let imgSrc = '/img/otra-imagen.png';
  if (imc < 18.5) {
    if (genero === 'sexoMasculino') {
      imgSrc = '/img/01H.png';
    } else if (genero === 'sexoFemenino') {
      imgSrc = '/img/01M.png';
    }
  } else if (imc >= 18.5 && imc <= 24.9) {
    if (genero === 'sexoMasculino') {
      imgSrc = '/img/02H.png';
    } else if (genero === 'sexoFemenino') {
      imgSrc = '/img/02M.png';
    }
  } else if (imc >= 25 && imc <= 29.9) {
    if (genero === 'sexoMasculino') {
      imgSrc = '/img/03H.png';
    } else if (genero === 'sexoFemenino') {
      imgSrc = '/img/03M.png';
    }
  } else if (imc >= 30 && imc <= 34.9) {
    if (genero === 'sexoMasculino') {
      imgSrc = '/img/04H.png';
    } else if (genero === 'sexoFemenino') {
      imgSrc = '/img/04M.png';
    }
  } else if (imc >= 35 && imc <= 39.9) {
    if (genero === 'sexoMasculino') {
      imgSrc = '/img/05H.png';
    } else if (genero === 'sexoFemenino') {
      imgSrc = '/img/05M.png';
    }
  } else if (imc >= 40) {
    if (genero === 'sexoMasculino') {
      imgSrc = '/img/06H.png';
    } else if (genero === 'sexoFemenino') {
      imgSrc = '/img/06M.png';
    }
  }

  let calorias;

if (edad >= 10 && edad < 18) {
  if (genero === 'sexoMasculino') {
    calorias = 17.686 * peso + 658.2;
  } else if (genero === 'sexoFemenino') {
    calorias = 13.384 * peso + 486.6;
  }
} else if (edad >= 18 && edad < 30) {
  if (genero === 'sexoMasculino') {
    calorias = 15.057 * peso + 692.2;
  } else if (genero === 'sexoFemenino') {
    calorias = 14.818 * peso + 486.6;
  }
} else if (edad >= 30 && edad < 60) {
  if (genero === 'sexoMasculino') {
    calorias = 11.472 * peso + 873.1;
  } else if (genero === 'sexoFemenino') {
    calorias = 8.126 * peso + 845.6;
  }
} else if (edad >= 60) {
  if (genero === 'sexoMasculino') {
    calorias = 11.711 * peso + 587.1;
  } else if (genero === 'sexoFemenino') {
    calorias = 9.082 * peso + 658.5;
  }
}
  ventanaResultado.document.write(`<h1 style="font-family: 'Roboto';">Calorias ${calorias.toFixed(2)} </h1>`);
  ventanaResultado.document.write(`<img src="${imgSrc}" alt="Resultado del IMC" style="position: absolute; top: 100px; right: 1000px;">`);
  document.getElementById("txtEdad").value = "";
  document.getElementById("txtPeso").value = "";
  document.getElementById("txtAltura").value = "";
});